from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from .models import Status, Task


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email')


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = '__all__'
        extra_kwargs = {
            'id': {'validators': []},
            'name': {'validators': []},
        }


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'

    status = StatusSerializer(many=False, required=False)
    user = UserSerializer(many=False, required=False)


    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        validated_data['user'] = user
        validated_data['status'] = get_object_or_404(Status, id=1)
        instance = Task.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        try:
            status = get_object_or_404(Status, id=validated_data.get('status', {}).get('id', 0))
        except:
            status = instance.status
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.status = status
        instance.save()
        return instance
