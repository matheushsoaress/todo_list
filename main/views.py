from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from .serializers import TaskSerializer
from .models import Task


@api_view(['GET'])
@permission_classes([AllowAny])
def index(request):
    return Response("Seja bem-vindo ao gerenciador de tarefas.", status=status.HTTP_200_OK)


@api_view(['GET', 'POST'])
def task(request):
    if request.method == 'POST':
        serializer = TaskSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'GET':
        task = Task.objects.filter(user=request.user).all()
        serializer = TaskSerializer(task, many=True)
        return Response(serializer.data)


@api_view(['DELETE', 'GET', 'PUT'])
def task_detail(request, id):
    try:
        task = Task.objects.filter(user=request.user).get(id=id)
    except Task.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'DELETE':
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    if request.method == 'GET':
        serializer = TaskSerializer(task)
        return Response(serializer.data)
    if request.method == 'PUT':
        serializer = TaskSerializer(task, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)