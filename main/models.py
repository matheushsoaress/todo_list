from django.db import models
from django.db.models import IntegerField, AutoField, CharField, TextField, ForeignKey, BooleanField, DateTimeField
from django.contrib.auth.models import User


class Status(models.Model):
    id = IntegerField(primary_key=True, null=False)
    name = CharField(max_length=255)
    description = TextField(null=True)


class Task(models.Model):
    id = AutoField(primary_key=True)
    user = ForeignKey(User, on_delete=models.CASCADE, related_name="user_id", null=True)
    status = ForeignKey(Status, on_delete=models.CASCADE, related_name="status_id", null=True)
    title = CharField(max_length=255)
    description = TextField(null=True)
    closed = BooleanField(default=False, null=True)
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)